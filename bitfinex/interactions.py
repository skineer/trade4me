import requests
from globalConfig import *
import json
import re
from externalQueueManager.externalQueueFunctions import initConnection, killConnection, sendMessage
import time
import random
from dateFunctions.dateFunctions import convertMilliToDate, convertDateToMilli
from pika.exceptions import ProbableAccessDeniedError, IncompatibleProtocolError, ProbableAuthenticationError
from urllib3.exceptions import ProtocolError, MaxRetryError
from requests.exceptions import ConnectionError, ChunkedEncodingError
from ssl import SSLEOFError
from proxy.proxyFunctions import getRandomProxy
from itertools import cycle
from http.client import IncompleteRead
from json.decoder import JSONDecodeError

proxies = getRandomProxy()
proxyPool = cycle(proxies)

def getAllValidSymbols():
    validSymbols = requests.get(VALIDSYMBOLSURL).text
    return json.loads(validSymbols)

def filterSymbolsUSD(validSymbols):
    regex = re.compile('\w{3}usd')
    selectedValues = list(filter(regex.search, validSymbols))
    return selectedValues

def getHistoricalData(dataCaptureQueue, dateControlObject, generalLog):
    # EXPECTED QUEUE MESSAGE:
    # (symbolKey, limit, startMs, endMs)
    while True:
        incoming = dataCaptureQueue.get()
        symbolKey = incoming[0]
        dateControlObject[symbolKey].update({'inProcess' : True})
        limit = incoming[1]
        startMs = incoming[2]
        endMs = incoming[3]
        randomTimeWait = random.randint(1, 1)
        time.sleep(randomTimeWait)
        formattedUrl = HISTDATACAPTURE.format(symbolKey.upper(), limit, startMs, endMs)
        try:
            proxy = next(proxyPool)
            currencyValues = json.loads(requests.get(formattedUrl, proxies = {'http' : proxy, 'https': proxy}).text)
        except (ConnectionError, ProtocolError, SSLEOFError, MaxRetryError,
                IncompleteRead, ChunkedEncodingError, JSONDecodeError):
            generalLog.write('REQUESTS ERROR\n')
            dateControlObject[symbolKey].update({'inProcess': False})
            dataCaptureQueue.task_done()
            continue
        # print(currencyValues)
        if isinstance(currencyValues, dict):
            dateControlObject[symbolKey].update({'inProcess': False})
            dataCaptureQueue.task_done()
            continue
        if not currencyValues:
            dateControlObject[symbolKey].update({'inProcess': 'Done'})
            generalLog.write('PROCESS COMPLETED FOR {0}\n'.format(symbolKey))
            dataCaptureQueue.task_done()
            continue
        currencyValues = [[convertMilliToDate(int(x[0])), x[1], x[2], x[3], x[4], x[5]] for x in currencyValues]
        try:
            channel, connection = initConnection()
        except (ProbableAccessDeniedError, IncompatibleProtocolError, ProbableAuthenticationError):
            generalLog.write('FAILED TO CONNECT TO RABBITMQ\n')
            dateControlObject[symbolKey].update({'inProcess': False})
            dataCaptureQueue.task_done()
            continue
        #writting data into external queue
        for item in currencyValues:
            sendMessage(channel = channel, exchangeName = '{0}_exchange'.format(symbolKey), message = str(item))
        killConnection(connection = connection)
        dateControlObject[symbolKey].update({'start' : convertDateToMilli(currencyValues[-1][0]) + 1})
        generalLog.write('SYMBOL : {0} UPDATED TO: {1}\n'.format(symbolKey, convertDateToMilli(currencyValues[-1][0])))
        dateControlObject[symbolKey].update({'inProcess': False})
        diff = abs(dateControlObject[symbolKey]['start'] - dateControlObject[symbolKey]['end'])
        generalLog.write('SYMBOL : {0} {1} DIFF: {2}\n'.format(symbolKey, dateControlObject[symbolKey], diff))
        dataCaptureQueue.task_done()

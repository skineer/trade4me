
# QUEUE CONFIGURATION SECTION
QUEUEHOST = 'localhost'
QUEUEPORT = 5672
QUEUEVIRTUALHOST = '/'
QUEUEUSER = 'guest'
QUEUEPASS = 'guest'

#BITFINEX CONFIGURATION SECTION
VALIDSYMBOLSURL = 'https://api.bitfinex.com/v1/symbols'
HISTDATACAPTURE = 'https://api-pub.bitfinex.com/v2/candles/trade:1m:t{0}/hist?limit={1}&start={2}&end={3}&sort=1'
DEFAULTINITDATE = '2000-01-01 00:00:00'

#LOGGING
LOGFILEPATH = '/Users/rpdr/trade4me/log/generaLog.txt'


# DELTA TIME TO STOP HISTORICAL DATA
DELTATIME = 4 * 60 * 60 * 1000
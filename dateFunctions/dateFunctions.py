import datetime

def convertDateToMilli(dateString):
    # YYYY-MM-DD HH:MM:SS
    dateObject = datetime.datetime.strptime(dateString, '%Y-%m-%d %H:%M:%S')
    millisec = dateObject.timestamp() * 1000
    return int(millisec)


def convertMilliToDate(ms):
    date = datetime.datetime.fromtimestamp(ms / 1000.0)
    dateObject = date.strftime('%Y-%m-%d %H:%M:%S')
    return dateObject

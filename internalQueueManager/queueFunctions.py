import queue

def createQueueInternal(maxsize = 0):
    return queue.Queue(maxsize = maxsize)

from bitfinex.interactions import getAllValidSymbols, filterSymbolsUSD, getHistoricalData
from externalQueueManager.externalQueueFunctions import initConnection, createExchange, \
                                              createQueueExternal, bindExchangeQueue, killConnection
from globalConfig import *
from dateFunctions.dateFunctions import convertDateToMilli
from internalThreading.threadingFunctions import createWorkers
from internalQueueManager.queueFunctions import createQueueInternal
import datetime
from itertools import cycle
import time

# TODO: the date control must start from the previous acquired data not from 2000-01-01 all the time
dateControlObject = {}
generalLog = open(LOGFILEPATH, 'a')
generalLog.write('PROCESS STARTED : {0}\n'.format(str(datetime.datetime.now())))
# get all valid symbols and filter it to only capture USD criptos
validSymbols = getAllValidSymbols()
filteredSymbols = filterSymbolsUSD(validSymbols)
generalLog.write('ASSETS ANALYZED: {0}\n'.format(str(filteredSymbols)))
# create external queues to dam the data
channel, connection = initConnection()
for item in filteredSymbols:
    exchangeName = '{0}_exchange'.format(item)
    queueName = '{0}_queue'.format(item)
    createExchange(exchangeName = exchangeName)
    createQueueExternal(queueName = queueName)
    bindExchangeQueue(queueName = queueName, exchangeName = exchangeName)
    startMs = convertDateToMilli(DEFAULTINITDATE)
    endMs   = convertDateToMilli(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    dateControlObject.update({item : {'start' : startMs, 'end' : endMs, 'inProcess' : False}})
killConnection(connection = connection)

# create internal queues
dataCaptureQueue = createQueueInternal()
# starting crawling threads
createWorkers(numThreads = len(filteredSymbols), targetFunction = getHistoricalData,
              args = (dataCaptureQueue, dateControlObject, generalLog))
generalLog.write('TOTAL THREADS: {0}\n'.format(str(len(filteredSymbols))))
symbolCycle = cycle(filteredSymbols)
while [x['inProcess'] for x in dateControlObject.values()] != ['Done' for x in range(0, len(dateControlObject.keys()))]:
    symbol = next(symbolCycle)
    if abs(dateControlObject[symbol]['start'] - dateControlObject[symbol]['end']) <= DELTATIME and \
    dateControlObject[symbol]['inProcess'] != 'Done':
        dateControlObject[symbol].update({'inProcess': 'Done'})
        generalLog.write('PROCESS COMPLETED FOR {0}\n'.format(symbol))
    if dateControlObject[symbol]['inProcess'] is False and dateControlObject[symbol]['inProcess'] != 'Done':
        dateControlObject[symbol].update({'end': convertDateToMilli(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))})
        dataCaptureQueue.put((symbol, 5000, dateControlObject[symbol]['start'], dateControlObject[symbol]['end']))
        generalLog.flush()
        time.sleep(0.25)

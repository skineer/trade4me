import threading
def createWorkers(numThreads, targetFunction, args):
    for z in range(numThreads):
        worker = threading.Thread(target = targetFunction, args = args)
        worker.setDaemon(True)
        worker.start()

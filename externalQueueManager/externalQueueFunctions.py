import pika
from globalConfig import *


def initConnection():
    credentials = pika.credentials.PlainCredentials(username = QUEUEUSER, password = QUEUEPASS)
    parameters = pika.ConnectionParameters(host = QUEUEHOST,
                                           port = QUEUEPORT,
                                           virtual_host = QUEUEVIRTUALHOST,
                                           credentials = credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    return channel, connection


def killConnection(connection):
    connection.close()
    return 0


def createExchange(exchangeName, exchangeType = 'fanout'):
    # If the exchange already exists, rabbitmq won't do nothing
    channel, connection = initConnection()
    channel.exchange_declare(exchange = exchangeName, exchange_type = exchangeType)
    stopConnection = killConnection(connection)
    return 0

def createQueueExternal(queueName):
    # If the queue already exists, rabbitmq won't do nothing
    channel, connection = initConnection()
    channel.queue_declare(queue = queueName)
    stopConnection = killConnection(connection)
    return 0


def bindExchangeQueue(queueName, exchangeName):
    channel, connection = initConnection()
    channel.queue_bind(exchange = exchangeName, queue = queueName)
    stopConnection = killConnection(connection)
    return 0

def sendMessage(channel, exchangeName, message):
    channel.basic_publish(exchange = exchangeName, routing_key = '', body = message)
    return 0